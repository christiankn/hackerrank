package Algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Warmup {
    public void compareTheTriplets(){
        Scanner in = new Scanner(System.in);
        int A_score = 0;
        int B_score = 0;
        int[] A = new int[3];
        int[] B = new int[3];
        for (int i = 0; i < 3; i++) {
            A[i] = in.nextInt();
        }
        for (int i = 0; i < 3; i++) {
                B[i] =   in.nextInt();
        }
        for (int i = 0; i < 3; i++) {
            if(A[i] > B[i])
                A_score++;
            if(A[i] < B[i])
                B_score++;
        }
        System.out.println(A_score + " " + B_score);
    }

    public void miniMaxSum(){
        Scanner in = new Scanner(System.in);
        long[] input = new long[5];
        long min = Integer.MAX_VALUE;
        long max = Integer.MIN_VALUE;
        for (int i = 0; i < 5; i++) {
            input[i] = in.nextInt();
        }
        for (int i = 0; i < 5; i++) {

            long sum = 0;
            for (int j = 0; j < 5; j++) {
                sum += input[j];
            }
            sum = sum - input[i];
            if(sum < min)
                min = sum;
            if(sum > max)
                max = sum;
        }
        System.out.println(min + " " + max);
    }
    public void birthdayCakeCandles() {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int tallestCandle = 0;
        int count = 0;
        for (int i = 0; i < N; i++) {
            int height = in.nextInt();
            if (height > tallestCandle) {
                tallestCandle = height;
                count = 0;
            }
            if (height == tallestCandle) {
                count++;
            }
        }
        System.out.println(count);
    }
}